from tkinter import *

root = Tk()

e = Entry(root, width=50)
e.pack()
# This puts text in the box by default
e.insert(0, "Default text")


def my_click():
    # e.get gets the value in the box
    my_label = Label(root, text=f"You entered: {e.get()}")
    my_label.pack()


# Make a button and put it in root, with padding
myButton = Button(root, text="Click Me!", padx=50, pady=20, command=my_click, fg="#ffffff", bg="#000000")

myButton.pack()

root.mainloop()
