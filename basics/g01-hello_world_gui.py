from tkinter import *

# creat the root widget
root = Tk()

# create the label widget
myLabel1 = Label(root, text="Hello World")
myLabel2 = Label(root, text="My name is Tom Marks")
myLabel3 = Label(root, text="GUIs are cool!")

# put it in the root widget, using the grid
myLabel1.grid(row=0, column=0)
myLabel2.grid(row=1, column=5)
myLabel3.grid(row=3, column=2)

# Can also be super efficient (maybe more complicated)
myLabel4 = Label(root, text="One line").grid(row=2, column=4)

# Create an event loop for the GUI to run
root.mainloop()
