from tkinter import *

root = Tk()


def my_click():
    my_label = Label(root, text="Look, I was clicked! :D")
    my_label.pack()


# Make a button and put it in root, with padding
myButton = Button(root, text="Click Me!", padx=50, pady=20, command=my_click, fg="#ffffff", bg="#000000")


myButton.pack()

root.mainloop()
